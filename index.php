<?php require_once "header.php"; ?>
<?php require_once 'includes/database.php'; ?>

<?php

/* Nemzetiség lekérése */
/* 1.Csatlakozok az adatbázishoz és lefuttatom az SQL kódot */
/* 2. Lement az eredményt egy változóban */
/* 3. Kiiratom az eredményt */

$sql_query = "SELECT * FROM nationalities";
$result = mysqli_query($connection, $sql_query);

$sql_query2 = "SELECT * FROM nationalities";
$result2 = mysqli_query($connection, $sql_query);

$sql_query3 = "SELECT * FROM languages";
$result3 = mysqli_query($connection, $sql_query3);


?>


<!-- Content Eleje -->

<div class="container mt-5">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="row text-center">
                        <h4 class="card-title">Registration</h4>
                    </div>
                    <form action="includes/registration.php" method="POST"> <!-- Method lehet GET vagy POST -->
                        <form>
                            <div class="mb-3">
                                <label for="Fullname" class="form-label">Name</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="Name">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputUsername1" class="form-label">Username</label>
                                <input type="text" class="form-control" id="Username" name="Username">
                                <div id="Username" class="form-text">It must contain a capital letter and number!
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">passwordConfirm</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" name="passwordConfirm">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" name="Email" aria-describedby="emailHelp">
                                <div id="emailHelp" class="form-text">We will never use you'r email!
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="Birthdate" class="form-label">Birthdate</label>
                                <input type="date" class="form-control" id="exampleInputDate1" name="Birthdate" >
                            </div>
                            <div class="mb-3">
                                <label for="Utcs" class="form-label">Postalcode</label>
                                <input type="number" class="form-control" id="exampleInputName1" name="Postalcode">
                            </div>
                            <div class="mb-3">
                                <label for="Utcs" class="form-label">Street</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="Street">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputOM1" class="form-label">OM ID</label>
                                <input type="Number" class="form-control" id="exampleInputOM1" name="Omid">
                            </div>
                            <div class="mb-3">
                                <label for="Fullname" class="form-label">Country</label>
                                <select name="Country" class="form-control" name="Country">
                                    <option value="#" disabled selected> Please choose</option>
                                    <?php
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="Fullname" class="form-label">Nationality</label>
                                <select name="Nationality" class="form-control">
                                    <option value="#" disabled selected> Please choose!</option>

                                    <?php
                                    while ($row = mysqli_fetch_assoc($result2)) {
                                        echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="Fullname" class="form-label">Languages</label>
                                <select name="Languages" class="form-control" >
                                    <option value="#" disabled selected> Please choose!</option>

                                    <?php
                                    while ($row = mysqli_fetch_assoc($result3)) {
                                        echo '<option value="' . $row["id"] . '">' . $row["language_name"] . '</option>';
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input name="Phone" type="text" placeholder="" data-mask="(36) (99)-999-9999" class="form-control">
                                <span class="font-13 text-muted">(36) 30-324-4252</span>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="Sex" id="sex" value="1">
                                <label class="form-check-label" for="">Male</label>
                            </div>
                            <div>
                                <input class="form-check-input" type="radio" name="Sex" id="sex" value="2">
                                <label class="form-check-label" for="">Female</label>
                            </div>
                            <div class="form-check">
                                <input name="Confirm" class="form-check-input" type="checkbox" id="Confirm">
                                <label class="form-check-label" for="confirm">Are you not a robot?</label>
                            </div>
                            <div class="mb-3"></div>
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-2">
    </div>
    </form>
</div>
</div>
<!-- Content vége -->
<?php require_once "footer.php"; ?>