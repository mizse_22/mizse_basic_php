<?php

/* 
    TODO: Pw table creation, Pw check, Pw hash, inserting pw table
    TODO: Address table creation, address table inserting, updating user table

    TODO: Need of $passwor_id, need of address_id

*/

if (isset($_POST['submit'])) {
    

require "database.php";


/* SQL VÉDELEM */
$name = mysqli_real_escape_string($connection, $_POST['Name']);
$username = mysqli_real_escape_string($connection, $_POST['Username']);
$password = mysqli_real_escape_string($connection, $_POST['Password']);
$passwordConfirm = mysqli_real_escape_string($connection, $_POST['PasswordConfirm']);
$email = mysqli_real_escape_string($connection, $_POST['Email']);
$birthdate = mysqli_real_escape_string($connection, $_POST['Birthdate']);
$Postalcode = mysqli_real_escape_string($connection, $_POST['Postalcode']);
$Street = mysqli_real_escape_string($connection, $_POST['Street']);
$addres_id = 1;
$omid = mysqli_real_escape_string($connection, $_POST['Omid']);
$nationality_id = mysqli_real_escape_string($connection, $_POST['Nationality']);
$language_id = mysqli_real_escape_string($connection, $_POST['Languages']);
$phone = mysqli_real_escape_string($connection, $_POST['Phone']);
$sex_id = mysqli_real_escape_string($connection, $_POST['Sex']);
$Human = mysqli_real_escape_string($connection, $_POST['Human']);


/* Jelszó validálás */
$uppercase = preg_match('@[A-Z]@', $password);
$lowercase = preg_match('@[a-z]@', $password);
$number    = preg_match('@[0-9]@', $password);
$specialChars = preg_match('@[^\w]@', $password);

echo "Átkérés sikerült <br>";

echo "SQL lekérdezés megkisérlése..<br>";

$sql_query = "INSERT INTO `users`(`name`, `username`, `password_id`, `birthdate`,
`phone`, `email`, `addres_id`, `omid`, `nationality_id`, `language_id`, `sex_id`) 
VALUES (?,?,?,?,?,?,?,?,?,?,?)";

$stmt = mysqli_stmt_init($connection);


$empty = array();
foreach ($_POST as $key => $value) {
    if (isset($value)=== false) {
        array_push($empty, $key);
    }
}

$emptyString = "?";

foreach ($empty as $key => $value) {
    $emptyString .= "empty_".$key."=".$value."&";
}

if (count($empty) !=0) {
    echo "Something is empty";
}
elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
    echo "Bad Email";
}
elseif (!$uppercase || 
        !$lowercase || 
        !$number || 
        !$specialChars || 
        strlen($password) < 8
        ) {
            echo 'Több karakter mindenféle védelemmel';
}
elseif ($password !== $passwordConfirm) {
    echo 'Nem egyező jelszavak';
};




mysqli_stmt_prepare($stmt, $sql_query);

if (mysqli_stmt_prepare($stmt, $sql_query)) {
    # code...
    mysqli_stmt_bind_param($stmt,'ssisssisiii',$name,$username,$password, $birthdate
    ,$phone,$email,$addres_id,$omid,$nationality_id,$language_id,$sex_id );
    
    mysqli_stmt_execute($stmt);

    echo "SQL lekérdezés sikerült";
}
else {
    echo "Statement fatal error";
}

}